#ifndef STANISLAUS_H
#define STANISLAUS_H

#include "stack.h"
#include <vector>

class Stanislaus
{

private:
    std::vector<stack<char>> levelStacks; // Stacks for each Level
    stack<int> arityStack;                // Stack for the arity
    stack<bool> booleanStack;             // Boolean values stack

    void negation()
    {
        std::cout << "Negating " << booleanStack.top() << std::endl;
        if (booleanStack.top() == false)
            booleanStack.top() = true;
        else
            booleanStack.top() = false;
    }

    void implication()
    {
        bool p = booleanStack.top();
        booleanStack.pop();
        bool q = booleanStack.top();
        booleanStack.pop();

        std::cout << "Implication of ";
        std::cout << p << " and " << q << std::endl;

        if (!p)
            booleanStack.push(true);
        else
            booleanStack.push(q);
    }

    void conjunction()
    {
        bool p = booleanStack.top();
        booleanStack.pop();
        bool q = booleanStack.top();
        booleanStack.pop();

        std::cout << "Conjunction of ";
        std::cout << p << " and " << q << std::endl;

        booleanStack.push(p && q);
    }

    void disjunction()
    {
        bool p = booleanStack.top();
        booleanStack.pop();
        bool q = booleanStack.top();
        booleanStack.pop();

        std::cout << "Disjunction of ";
        std::cout << p << " and " << q << std::endl;

        booleanStack.push(p || q);
    }

public:
    // Constructor
    Stanislaus() : levelStacks(), arityStack(), booleanStack()
    {

        // Create 9 levels in the vector
        levelStacks.resize(9);
    }

    void push(char formVal, int arityVal)
    {

        levelStacks.at(arityVal).push(formVal);
        arityStack.push(arityVal);
    }

    bool runALGOL()
    {

        // Step 1
        bool solution = true;

        // Step 2
        while (!arityStack.empty())
        {

            int stackVal = arityStack.top(); // Step 3

            std::cout << "Accessing " << stackVal << ".";

            char levelVal = levelStacks.at(stackVal).top(); // Step 4

            std::cout << "Top value is " << levelVal << std::endl;

            // Step 5 Use the levelVal to determine the next action
            switch (levelVal)
            {

            case '0': // Step 5-1
                booleanStack.push(false);
                break;

            case '1': // Step 5-2
                booleanStack.push(true);
                break;

            case 'N': // Step 5-3
                negation();
                break;

            case 'I': // Step 5-4
                implication();
                break;

            case 'C': // Step 5-5
                conjunction();
                break;

            case 'D': // Step 5-6
                disjunction();
                break;
            };

            // End of Step 2 - Pop those elements of the stacks
            arityStack.pop();
            levelStacks.at(stackVal).pop();
        }

        // Step 6
        solution = booleanStack.top();
        booleanStack.pop();

        // Step 7
        return solution;
    }
};

#endif
