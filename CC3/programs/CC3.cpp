/*************************************
 * File name: CC3.cpp
 * Author: Pablo Martinez Abrego
 * Email: pmarti22@nd.edu
 *
 * Given a set of inputs from the user, perform a
 * set of rotations on a array of integers
 *
 * ***********************************/

 #include <iostream>
 #include "../classes/DLList.h"

/*************************
 * Function Name: getInputConstr
 * Preconditions: int&, int&, char&
 * Postconditions: none
 * Gets input from the user and determines the length, rotations, and direction
 * **********************/
void getInputConstr(unsigned int& length, unsigned int& rotations, char& cDirection){

    std::cout << "-----------------------------------------------------------------------" << std::endl;
    std::cout << "Enter the length, number of rotations, and the direction (L/R)." << std::endl;
    std::cout << "e.g.: length = 5, number of rotations = 4, and the direction is left..." << std::endl;
    std::cout << "Then the desired input is: 5 4 L" << std::endl;
    std::cout << "Enter your values: ";

    std::cin >> length;
    std::cin >> rotations;
    std::cin >> cDirection;

}


/*************************
 * Function Name: main
 * Preconditions: none
 * Postconditions: int
 * This is the main driver function
 * **********************/
int main()
{
    // Rotation char loop
    char loopCont = 'y';

    while(loopCont == 'y'){

		// Set the Initial input variables
		unsigned int length, rotations;
		char cDirection;

		// Get the input constraints
		getInputConstr(length, rotations, cDirection);

    std::cout << "User entered: Length = " << length;
		std::cout << ", NumRotations = " << rotations;
		std::cout << ", and will rotate in " << cDirection << " direction.\n";

		// Student algorithm will go here

    // Initialize linked List
    DLList<int> intList;
    int value;

    // Ask for user input and save in list
    std::cout << "Type in " << length << " integers: ";
    for(unsigned int i = 0; i < length; i++){
      std::cin >> value;
      intList.Insert(value);
    }
    std::cout << std::endl;

    // Print initial list
    std::cout << "Initial List: " << intList << std::endl;

    // Call shift method
    intList.shift(length, rotations, cDirection);

    // Print rotated list
    std::cout << "Rotated List: " << intList << std::endl;

		// Finish the loop
        std::cout << "Continue? Enter y/n: ";
        std::cin >> loopCont;

    }

    std::cout << "...Exiting Program ..." << std::endl;

    return 0;
}
