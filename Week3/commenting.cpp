/**********************************************
* File: commenting.cpp
* Author: Pablo Martinez Abrego
* Email: pmarti22@nd.edu
* File for practicing commenting 
**********************************************/

#include <iostream>

/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
* This is the main driver 
********************************************/
int main(){
	std::cout << "Hello, world!" << std::endl;

	return 0;
}

