/**********************************************
* File: stringRem.cpp
* Author: Pablo Martinez Abrego
* Email: pmarti22@nd.edu
* Remove duplicates from string 
**********************************************/

#include <iostream>
#include <string>

// Declaring function
std::string uniqueStr(std::string);

/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
* This is the main driver 
********************************************/
int main(){

	std::string strTest1("adagahljASDj67SAl7");
	std::string strNoDupes("abcdABCD1234");

	// Print initial values
	std::cout << "Before Removing Duplicates\n";
	std::cout << strTest1 << char(0x0A);
	std::cout << strNoDupes << "\n\n";

	// Print final values
	std::cout << "After Removing Duplicates:" << (char)10;
	std::cout << uniqueStr(strTest1) << std::endl;
	std::cout << uniqueStr(strNoDupes) << std::endl;

	return 0;
}


/********************************************
* Function Name  : uniqueStr
* Pre-conditions : std::string str
* Post-conditions: std::string
* This is the function that eliminates the duplicates
* of string, returning another string 
********************************************/
std::string uniqueStr(std::string str)
{
	// Array full of 127 0s
	int ascii[127];
	for(int i = 0; i < 126; i++){
		ascii[i] = 0;
	}

	// Finding length of string
	int len = 0;
	while(str[len] != char(0)){	
		len++;
	}

	// Temporary string
	std::string temp;

	int count = 0;
	
	// Finding unique strings
	for(int i = 0; i < len; i++){
		if(ascii[(int)str[i]] == 0){
			temp.push_back(str[i]);
			ascii[(int)str[i]] = 1;
			count++;
		}
		// If all unique strings are found, return string
		if(count == 127)
			return temp;
	}

	// Return temp string at the end
	return temp;

}
