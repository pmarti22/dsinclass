#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include "Vertex.h"
#include <iostream>
#include <fstream>
#include <stack>
#include <queue>
#include "MSTElem.h"
#include "Edge.h"
#include <unordered_map>

template<class T>
class Graph{

	private:

		std::vector< Vertex< T > > vertices;
		bool bi_directional;

	public:

		Graph( bool bi_direct ) : vertices(), bi_directional(bi_direct) {}


		void add_vertex( T vertexData ){

			Vertex<T> theVertex( vertexData );

			vertices.push_back( theVertex );

		}


		void add_edge(unsigned int origin, unsigned int destin, int weight ){

			if( origin < vertices.size() && destin < vertices.size() ){

				vertices.at(origin).add_edge( destin, weight );

				if( bi_directional ){

					vertices.at(destin).add_edge( origin, weight );
				}
			}
			else{
				std::cout << "Unable to add edge. Edge outside graph bounds\n";
			}
		}


		bool get_vertex_value( unsigned int vertex, T& value ){

			bool validVertex = false;

			if( vertex < vertices.size() ){

				validVertex = true;
				value = vertices.at(vertex).get_vertex_value();

			}

			return validVertex;

		}


		void set_vertex_value( unsigned int vertex, T& value ){

			if( vertex < vertices.size() ){

				value = vertices.at(vertex).set_vertex_value( value );

			}

		}

		bool adjacent( unsigned int origin, unsigned int destin ){

			if( origin < vertices.size() && destin < vertices.size() ){

				for(unsigned int i = 0; i < vertices.at(origin).num_edges(); i++){

					if(vertices.at(origin).get_edge().destin == destin){

						return true;
					}
				}

				return false;
			}
			else{

				return false;
			}

		}


		void neighbors( unsigned int vertex ){

			if( vertex < vertices.size() ){

				std::cout << "Vertex " << vertex << " had edges going to: ";

				for(unsigned int i = 0; i < vertices.at(vertex).num_edges(); i++){

					std::cout << vertices.at(vertex).get_edge().destin << ", ";

				}
				std::cout << std::endl;

			}
			else{

				std::cout << vertex << " exceeds graph capacity\n";
			}

		}


		bool get_edge_value( unsigned int origin, unsigned int destin,
			int& weight){

			if( origin < vertices.size() && destin < vertices.size() ){

				vertices.at(origin).get_edge_value( destin, weight );
				return true;

			}

			return false;

		}

		bool set_edge_value( unsigned int origin, unsigned int destin,
			int weight){

			if( origin < vertices.size() && destin < vertices.size() ){

				vertices.at(origin).set_edge_value( destin, weight );

				if( bi_directional ) {

					vertices.at(destin).set_edge_value( origin, weight );

				}

				return true;

			}

			return false;

		}


		void remove_edge( unsigned int origin, unsigned int destin){

			if( origin < vertices.size() && destin < vertices.size() ){

				vertices.at(origin).remove_edge( destin );

				if( bi_directional ) {

					vertices.at(destin).remove_edge( origin );

				}

				return;

			}

			return;

		}


		friend std::ostream& operator<<( std::ostream& out, const Graph<T>& g){

			for( unsigned int i = 0; i < g.vertices.size(); i++ ){

				out << "{" << i << ", " << g.vertices.at(i).get_vertex_value() << "} ";

				for( unsigned int j = 0; j < g.vertices.at(i).num_edges(); j++ ){

					Edge tempEdge = g.vertices.at(i).get_edge(j);

					std::cout << "(" << tempEdge.destin << ", ";
					std::cout << tempEdge.weight << "), ";
				}

				out << std::endl;
			}

			return out;
		}

		void remove_vertex( unsigned int vertexLoc ){

			if( vertexLoc < vertices.size() ){

				vertices.erase( vertices.begin() + vertexLoc );

				for(unsigned int i = 0; i < vertices.size(); i++){

					vertices.at(i).removeAndUpdateEdges( vertexLoc );
				}

			}

		}

		// Public Method inside Graph.h
		void BFS( unsigned int destin ){

			// Check if the graph is valid to be searched
			if( destin < vertices.size() && vertices.size() > 0){

				// Structures to be used for the BFS Traversal
				std::queue< unsigned int > theQueue;
				unsigned int* parents = new unsigned int[vertices.size()];
				bool* visited = new bool[vertices.size()];
				std::stack< unsigned int > finalPath;

				// Initialize the search
				bool found = false;
				theQueue.push(0);
				parents[0] = -1;
				visited[0] = true;

				// Code Segment 34-5 will go here
				if( destin == 0 ){

					found = true;
					finalPath.push( 0 );

				}

				// Code Segment 34-6 will go here
				// Step 1
				while( !found && theQueue.size() < vertices.size() ){

					// Step 2
					unsigned int vertex = theQueue.front();
					theQueue.pop();

					// Step 3
					for( unsigned int i = 0; i < vertices.at( vertex ).num_edges(); i++){

						// Step 4
						Edge tempEdge = vertices.at( vertex ).get_edge( i );

						// Step 5
						if( !visited[ tempEdge.destin ] ){
							// Step 5-1
							visited[ tempEdge.destin ] = true;

							// Step 5-2
							parents[ tempEdge.destin ] = vertex;

							// Step 5-3
							if( tempEdge.destin == destin ){
								found = true;
								finalPath.push( destin );
								break;
							}

							// Step 5-4
							else{
								theQueue.push(tempEdge.destin);
							}
						}
					}
				}

				// Code Segment 34-7 will go here
				// Step 1
				if( found ){

					// Step 2
					unsigned int parent = parents[ destin ];

					while( parent != -1 ){

						finalPath.push( parent );
						parent = parents[ parent ];

					}

					std::cout << "The path from " << vertices.at( 0 ).get_vertex_value();
					std::cout << " to " << vertices.at(destin).get_vertex_value() << " is: ";

					// Step 3
					while(!finalPath.empty()){

						std::cout << vertices.at( finalPath.top() ).get_vertex_value() << ", ";
						finalPath.pop();
					}

					std::cout << std::endl;
				}

				else{

					std::cout << "No Path Exists\n";

				}

			}
			else{
				std::cout << "Vertex " << destin << " is not a valid vertex in the graph\n";
			}
		}

		Graph<T> MST() {

			Graph<T> MSTGraph( false );

			std::priority_queue< MSTElem > MST_PQ;
			std::unordered_map< unsigned int, bool > frontier;
			unsigned int* parents = new unsigned int[ vertices.size() ];
			int* weights = new int[ vertices.size() ];

			MSTElem origin( 0, 0 );
			MST_PQ.push( origin );
			frontier.insert( {0, true} );
			parents[0] = -1;
			weights[0] = -2147483648;

			for( unsigned int i = 1; i < vertices.size(); i++){

				MSTElem temp( i, 2147483647 );
				weights[i] = 2147483647;
				MST_PQ.push( temp );
				frontier.insert( {i, true} );
			}

			while( !MST_PQ.empty() ){

				MSTElem currElem = MST_PQ.top();

				Vertex<T> currVert = vertices.at( currElem.index );

				MST_PQ.pop();

				frontier[ currElem.index ] = false;

				for( unsigned int i = 0; i < currVert.num_edges(); i++){

					Edge currEdge = currVert.get_edge( i );

					if( currEdge.weight < weights[ currEdge.destin ] && frontier[ currEdge.destin ] ){

						weights[ currEdge.destin ] = currEdge.weight;
						parents[ currEdge.destin ] = currElem.index;

						MSTElem pushElem( currEdge.destin, currEdge.weight);

						MST_PQ.push( pushElem );
					}
				}
			}

			for( unsigned int i = 0; i < vertices.size(); i++){

				MSTGraph.add_vertex( vertices.at(i).get_vertex_value() );
			}

			for( unsigned int i = 0; i < vertices.size(); i++){

				if( parents[i] != -1 ){

					MSTGraph.add_edge( parents[i], i, weights[i] );
				}
			}

			return MSTGraph;
		}

};

#endif
