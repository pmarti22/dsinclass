#include "../classes/Graph.h"
#include <iostream>
#include <vector>

const int RIVER_BANK_NORTH = 0;
const int LEFT_ISLAND = 1;
const int RIGHT_ISLAND = 2;
const int RIVER_BANK_SOUTH = 3;

int main(){
	
	Graph< std::string > Koningsberg( true );
	
	Koningsberg.add_vertex( "River Bank North:" );
	Koningsberg.add_vertex( "Left Island     :" );
	Koningsberg.add_vertex( "Right Island    :" );
	Koningsberg.add_vertex( "River Bank South:" );
	
	// Nodes from River Bank North
	Koningsberg.add_edge( RIVER_BANK_NORTH, LEFT_ISLAND, 1 );
	Koningsberg.add_edge( RIVER_BANK_NORTH, RIGHT_ISLAND, 1 );
	
	// Nodes from Left Island
	Koningsberg.add_edge( LEFT_ISLAND, RIVER_BANK_SOUTH, 1 );
	Koningsberg.add_edge( LEFT_ISLAND, RIGHT_ISLAND, 1 );
	
	// Nodes from Right Island
	Koningsberg.add_edge( RIGHT_ISLAND, RIVER_BANK_NORTH, 1 );
	Koningsberg.add_edge( RIGHT_ISLAND, RIVER_BANK_SOUTH, 1 );
	
	// Nodes from River Bank South
	Koningsberg.add_edge( RIVER_BANK_SOUTH, RIGHT_ISLAND, 1 );

	
	// Print Original Graph:
	std::cout << "Original Koningsberg Graph:\n" << Koningsberg << std::endl;
	
	// Run Breadth-First Search
	Graph< std::string > MSTGraph = Koningsberg.MST();

	std::cout << "\nMST of Koningsberg:\n" << MSTGraph << std::endl;

	return 0;
}