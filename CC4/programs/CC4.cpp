/*************************************
 * File name: CC4.cpp
 * Author: Pablo Martinez Abrego
 * Email: pmarti22@nd.edu
 *
 * Find the least common ancestor for a BST
 *
 * ***********************************/

#include "../classes/BST.h"

/*************************
 * Function Name: fillTree
 * Preconditions: int&, int&, char&
 * Postconditions: none
 * Gets input from the user and determines the length, rotations, and direction
 * **********************/
void fillTree(BST<int>& theTree){

	unsigned int numElements;

    std::cout << "-----------------------------------------------------------------------" << std::endl;
    std::cout << "First, enter the number of elements in the tree: ";
    std::cin >> numElements;

	std::cout << "\nNext, insert " << numElements << " into the tree: ";

	for(unsigned int i = 0; i < numElements; i++){

		int temp;
		std::cin >> temp;

		theTree.Insert(temp);
	}

	std::cout << "\nThe initial tree is: \n";

	theTree.printInOrder(std::cout);

	theTree.printPreOrder(std::cout);

	theTree.printPostOrder(std::cout);

}



/*************************
 * Function Name: main
 * Preconditions: none
 * Postconditions: int
 * This is the main driver function
 * **********************/
int main()
{

	// Create the Binary Search Tree
	BST<int> theTree;

	// Fill the Tree
	fillTree( theTree );

  // Rotation char loop
  char loopCont = 'y';

	int firstElem, secondElem;

	while(loopCont == 'y'){


			std::cout << "\nInsert a pair to check - [First] [Second]: ";
			std::cin >> firstElem >> secondElem;

			while(!(theTree.contains(firstElem)) || !(theTree.contains(secondElem))){
				std::cout << "Invalid Element. Try again: ";
				std::cin >> firstElem >> secondElem;
			}

			std::cout << "\nFinding LCA for " << firstElem << " and " << secondElem << "...\n";

			std::cout << theTree.ancestor(firstElem, secondElem) << std::endl;

			std::cout << "\nContinue? Enter y/n: ";
			std::cin >> loopCont;
	}

    std::cout << "...Exiting Program ..." << std::endl;

    return 0;
}
