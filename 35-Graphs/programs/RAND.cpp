#include "../classes/Graph.h"
#include <iostream>
#include <vector>

int main(){
	
	Graph< int > theGraph( true );
	
	theGraph.add_vertex( 1 );
	theGraph.add_vertex( 2 );
	theGraph.add_vertex( 3 );
	theGraph.add_vertex( 4 );
	theGraph.add_vertex( 5 );
	theGraph.add_vertex( 6 );
	theGraph.add_vertex( 7 );
	theGraph.add_vertex( 8 );
	
	// Nodes from 0
	theGraph.add_edge( 0, 1, 7 );
	theGraph.add_edge( 0, 2, 1 );
	theGraph.add_edge( 0, 3, 1 );

	// Nodes from 1	
	theGraph.add_edge( 1, 2, 1 );
	theGraph.add_edge( 1, 4, 2 );
	
	// Nodes from 2
	theGraph.add_edge( 2, 3, 3 );
	theGraph.add_edge( 2, 4, 1 );
	theGraph.add_edge( 2, 5, 1 );
	theGraph.add_edge( 2, 6, 5 );
	
	// Nodes from 3 
	theGraph.add_edge( 3, 6, 4 );
	
	// Nodes from 4 
	theGraph.add_edge( 4, 5, 3 );
	theGraph.add_edge( 4, 7, 1 );
	
	// Nodes from 5 
	theGraph.add_edge( 5, 6, 1 );
	theGraph.add_edge( 5, 7, 1 );

	// Nodes from 6
	theGraph.add_edge( 6, 7, 8 );

	
	// Print results:
	std::cout << theGraph << std::endl;
	
	// Run Dijkstra's Algorithm
	theGraph.Dijkstra( 7 );
	theGraph.Dijkstra( 6 );
	theGraph.Dijkstra( 5 );
	theGraph.Dijkstra( 4 );
	theGraph.Dijkstra( 3 );
	theGraph.Dijkstra( 2 );
	theGraph.Dijkstra( 1 );
	theGraph.Dijkstra( 0 );
	return 0;
}