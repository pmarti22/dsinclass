#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include "Vertex.h"

#include <iostream>
#include <fstream>

#include <queue>
#include <stack>

#include "MSTElem.h"
#include <unordered_map>

template<class T>
class Graph{

	private:

		std::vector< Vertex< T > > vertices;
		bool bi_directional;

	public:

		Graph( bool bi_direct ) : vertices(), bi_directional(bi_direct) {}


		void add_vertex( T vertexData ){

			Vertex<T> theVertex( vertexData );

			vertices.push_back( theVertex );

		}


		void add_edge(unsigned int origin, unsigned int destin, int weight ){

			if( origin < vertices.size() && destin < vertices.size() ){

				vertices.at(origin).add_edge( destin, weight );

				if( bi_directional ){

					vertices.at(destin).add_edge( origin, weight );
				}
			}
			else{
				std::cout << "Unable to add edge. Edge outside graph bounds\n";
			}
		}


		bool get_vertex_value( unsigned int vertex, T& value ){

			bool validVertex = false;

			if( vertex < vertices.size() ){

				validVertex = true;
				value = vertices.at(vertex).get_vertex_value();

			}

			return validVertex;

		}


		void set_vertex_value( unsigned int vertex, T& value ){

			if( vertex < vertices.size() ){

				value = vertices.at(vertex).set_vertex_value( value );

			}

		}

		bool adjacent( unsigned int origin, unsigned int destin ){

			if( origin < vertices.size() && destin < vertices.size() ){

				for(unsigned int i = 0; i < vertices.at(origin).num_edges(); i++){

					if(vertices.at(origin).get_edge().destin == destin){

						return true;
					}
				}

				return false;
			}
			else{

				return false;
			}

		}


		void neighbors( unsigned int vertex ){

			if( vertex < vertices.size() ){

				std::cout << "Vertex " << vertex << " had edges going to: ";

				for(unsigned int i = 0; i < vertices.at(vertex).num_edges(); i++){

					std::cout << vertices.at(vertex).get_edge().destin << ", ";

				}
				std::cout << std::endl;

			}
			else{

				std::cout << vertex << " exceeds graph capacity\n";
			}

		}


		bool get_edge_value( unsigned int origin, unsigned int destin,
			int& weight){

			if( origin < vertices.size() && destin < vertices.size() ){

				vertices.at(origin).get_edge_value( destin, weight );
				return true;

			}

			return false;

		}

		bool set_edge_value( unsigned int origin, unsigned int destin,
			int weight){

			if( origin < vertices.size() && destin < vertices.size() ){

				vertices.at(origin).set_edge_value( destin, weight );

				if( bi_directional ) {

					vertices.at(destin).set_edge_value( origin, weight );

				}

				return true;

			}

			return false;

		}


		void remove_edge( unsigned int origin, unsigned int destin){

			if( origin < vertices.size() && destin < vertices.size() ){

				vertices.at(origin).remove_edge( destin );

				if( bi_directional ) {

					vertices.at(destin).remove_edge( origin );

				}

				return;

			}

			return;

		}


		void remove_vertex( unsigned int vertexLoc ){

			if( vertexLoc < vertices.size() ){

				vertices.erase( vertices.begin() + vertexLoc );

				for(unsigned int i = 0; i < vertices.size(); i++){
					vertices.at(i).removeAndUpdateEdges( vertexLoc );
				}

			}

		}




		void BFS( unsigned int destin ){

			// Check if the graph is valid to be searched
			if( destin < vertices.size() && vertices.size() > 0){

				std::queue< unsigned int > theQueue;
				//unsigned int* parents = (unsigned int*)malloc( vertices.size() * sizeof(unsigned int));
				unsigned int* parents = new unsigned int[vertices.size()];
				//bool* visited = (bool*)malloc( vertices.size() * sizeof(bool));
				bool* visited = new bool[vertices.size()];
				std::stack< unsigned int > finalPath;

				// Initialize the search
				bool found = false;
				theQueue.push(0);
				parents[0] = -1;
				visited[0] = true;

				if( destin == 0 ){

					found = true;
					finalPath.push( 0 );

				}

				// Conduct the search. If the queue's size matches the vertices
				// then every vertex has been visited.
				while( !found && theQueue.size() < vertices.size() ){

					unsigned int vertex = theQueue.front();
					theQueue.pop();

					for( unsigned int i = 0; i < vertices.at( vertex ).num_edges(); i++){

						Edge tempEdge = vertices.at( vertex ).get_edge( i );

						// State the vertex has been visited
						if( !visited[ tempEdge.destin ] ){
							visited[ tempEdge.destin ] = true;
							parents[ tempEdge.destin ] = vertex;

							if( tempEdge.destin == destin ){

								found = true;
								finalPath.push( destin );
								break;
							}
							else{
								theQueue.push(tempEdge.destin);
							}
						}
					}

				}


				if( found ){

					unsigned int parent = parents[ destin ];

					while( parent != -1 ){

						finalPath.push( parent );
						parent = parents[ parent ];

					}

					// Finally, print the BFS Path
					std::cout << "The path from " << vertices.at( 0 ).get_vertex_value();
					std::cout << " to " << vertices.at(destin).get_vertex_value() << " is: ";

					while(!finalPath.empty()){

						std::cout << vertices.at( finalPath.top() ).get_vertex_value() << ", ";

						finalPath.pop();
					}

					std::cout << std::endl;
				}

				else{

					std::cout << "No Path Exists\n";

				}

			}
			else{

				std::cout << "Vertex " << destin << " is not a valid vertex in the graph\n";

			}

		}

		void DFS( unsigned int destin ){

			// Check if the destin value is valid to be searched
			if( destin < vertices.size() && vertices.size() > 0){

				std::stack< unsigned int > theStack;
				std::vector<unsigned int> parents(vertices.size(), -1);
				std::vector<unsigned int> visited_edges(vertices.size(), 0);
				std::vector<bool> visited(vertices.size(), false);
				std::vector<int> weights(vertices.size());
				std::stack< unsigned int > finalPath;
				bool found = false;

				// Code Segment 35-2 Goes Here
				theStack.push(0);
				visited[0] = true;
				unsigned int visitedVerts = 1;

				// Code Segment 35-3 Goes Here
				if( destin == 0 ){
					found = true;
					finalPath.push( 0 );
				}

				// Code Segments 35-4 through 35-6 Go Here
				while( !found && visitedVerts < vertices.size()){

					unsigned int vertex = theStack.top();

					// Code Segment 5 goes here
					// Step 1
					if( vertices.at( vertex ).num_edges() > visited_edges[ vertex ] ){

						// Step 2
						Edge tempEdge = vertices.at( vertex ).get_edge( visited_edges[ vertex ] );
						visited_edges[ vertex ]++;

						// Step 3
						if( !visited[ tempEdge.destin ] ){

							// Step 4
							visited[ tempEdge.destin ] = true;
							parents[ tempEdge.destin ] = vertex;
							weights[ tempEdge.destin ] = tempEdge.weight;

							// Step 5-1
							if( tempEdge.destin == destin ){
								found = true;
								finalPath.push( destin );
								break;
							}
							// Step 5-2
							else{
								theStack.push(tempEdge.destin);
							}
							visitedVerts++;
						}
						// Step 5
						vertex = theStack.top();
					}

				// Code Segment 6 Goes Here
				if( visited[vertex] &&  visited_edges[vertex] == vertices.at( vertex ).num_edges() ){
					theStack.pop();
				}

				if( theStack.empty() ){
					break;
				}

			}

				// Code Segment 35-7 Goes Here
				// Step 1
				if( found ){

					// Step 2
					unsigned int parent = parents[ destin ];

					while( parent != -1 ){
						finalPath.push( parent );
						parent = parents[ parent ];
					}
					std::cout << "The path from " << vertices.at( 0 ).get_vertex_value();
					std::cout << " to " << vertices.at(destin).get_vertex_value() << " is: ";

					// Step 3
					while(!finalPath.empty()){
						std::cout << vertices.at( finalPath.top() ).get_vertex_value() << ", ";
						finalPath.pop();
					}
					std::cout << std::endl;
				}

			}
			else{
				std::cout << "Vertex " << destin << " is not a valid vertex in the graph\n";
			}
		}


		Graph<T> MST(){

			Graph<T> MSTGraph( false );

			std::priority_queue< MSTElem > MST_PQ;
			std::unordered_map< unsigned int, bool > frontier;
			int* weights = new int[vertices.size()];
			unsigned int* parents = new unsigned int[vertices.size()];

			// First, fill the Priority Queue with MSTElem values
			// Origin will be 0
			MSTElem origin( 0, 0 );
			MST_PQ.push( origin );
			frontier.insert( {0, true} );
			parents[ 0 ] = -1;
			weights[ 0 ] = -2147483648;

			// Make the other elements infinity
			for(unsigned int i = 1; i < vertices.size(); i++){

				MSTElem temp( i, 2147483647 );
				weights[ i ] = 2147483647;
				MST_PQ.push( temp );
				frontier.insert( {i, true} );
			}


			while( !MST_PQ.empty() ){

				MSTElem currElem = MST_PQ.top();
				Vertex<T> currVert = vertices.at( currElem.index );
				MST_PQ.pop();

				frontier[ currElem.index ] = false;

				for(unsigned int i = 0; i < currVert.num_edges(); i++){

					Edge currEdge = currVert.get_edge( i );

					if( currEdge.weight < weights[ currEdge.destin ] && frontier[ currEdge.destin ] ){

						weights[ currEdge.destin ] = currEdge.weight;

						parents[ currEdge.destin ] = currElem.index;

						MSTElem pushElem( currEdge.destin, currEdge.weight );

						MST_PQ.push( pushElem );
					}

				}
			}

			// Add the vertext values for each element
			for( unsigned int i = 0; i < vertices.size(); i++){

				MSTGraph.add_vertex( vertices.at(i).get_vertex_value() );

			}

			// Add the edges to the MSTGraph to be returned
			for( unsigned int i = 1; i < vertices.size(); i++){

				if( parents[i] != -1 ){

					MSTGraph.add_edge( parents[i], i, weights[i] );

				}
			}

			return MSTGraph;

		}

Graph<T> TopSort( ){

			// Create a templated graph, and set it uni-directional by putting in false
			Graph<T> topSort( false );

			std::stack< unsigned int > theStack;
			std::vector<unsigned int> parents(vertices.size(), -1);
			std::vector<unsigned int> visited_edges(vertices.size(), 0);
			std::vector<bool> visited(vertices.size(), false);
			std::vector<int> weights(vertices.size());

			// Remove the found bool and foundPath stack, since we will not need them.

			// Initialize the search
			theStack.push(0);
			parents[0] = -1;
			visited[0] = true;
			unsigned int visitedVerts = 0;

			// Remove the check if the origin is the sought node, since
			// we will traverse the entire graph

			// Remove found node from this while loop, since we traverse the entire graph
			while( visitedVerts < vertices.size() ){

				unsigned int vertex = theStack.top();

				if( vertices.at( vertex ).num_edges() > visited_edges[ vertex ] ){

					Edge tempEdge = vertices.at(vertex).get_edge(visited_edges[ vertex ]);

					visited_edges[ vertex ]++;

					// State the vertex has been visited
					if( !visited[ tempEdge.destin ] ){

						visited[ tempEdge.destin ] = true;
						parents[ tempEdge.destin ] = vertex;
						weights[ tempEdge.destin ] = tempEdge.weight;

						// Remove the if-else, and just push every time
						theStack.push(tempEdge.destin);

						visitedVerts++;

					}

					vertex = theStack.top();
				}


				if( visited_edges [vertex] == vertices.at( vertex ).num_edges() ){
					theStack.pop();
				}

				if( theStack.empty() ){

					// Code Segment 9 Goes Here
					if( visitedVerts < vertices.size() ){

						unsigned int temp = vertex + 1;

						while( visited[ temp ] ){

							temp++;
						}

						theStack.push( temp );
						visitedVerts++;

					}
					else{
						break;
					}
				}

			}

				// Code Segment 10 Goes Here
				// Add the vertices
				for( unsigned int i = 0; i < vertices.size(); i++){
					topSort.add_vertex( vertices.at(i).get_vertex_value() );
				}

				for( unsigned int i = 0; i < vertices.size(); i++){
					if( parents[i] != -1 ){
						topSort.add_edge( parents[i], i, weights[i] );
					}
				}

				return topSort;
		}

void Dijkstra(unsigned int destin){
			if(destin<vertices.size() && vertices.size()>0){
				std::stack<unsigned int> theStack;
				std::vector<unsigned int> parents(vertices.size(),-1);
				std::vector<int> distance(vertices.size(),2147483647);
				std::stack<unsigned int> finalPath;
				bool found = false;

				//Initialize Origin
				theStack.push(0);
				distance[0] = 0;

				if(destin == 0){
					found = true;
					finalPath.push(0);
				}

				while (!theStack.empty()) {

					unsigned int index = theStack.top();
					theStack.pop();

					for(unsigned int i = 0; i < vertices.at(index).num_edges(); i++){
						Edge tempEdge = vertices.at(index).get_edge(i);
						if(distance[index]+tempEdge.weight<distance[tempEdge.destin]){
							distance[tempEdge.destin] = distance[index]+tempEdge.weight;
							parents[tempEdge.destin] = index;

							if(tempEdge.destin == destin && !found){
								found = true;
								finalPath.push(destin);
							}
							theStack.push(tempEdge.destin);
						}
					}
				}

				if( found ){

					unsigned int parent = parents[ destin ];

					while( parent != -1 ){

						finalPath.push( parent );
						parent = parents[ parent ];

					}

					// Finally, print the Dijkstra
					std::cout << "The path from " << vertices.at( 0 ).get_vertex_value();
					std::cout << " to " << vertices.at(destin).get_vertex_value() << " is: ";

					while(!finalPath.empty()){

						std::cout << vertices.at( finalPath.top() ).get_vertex_value() << ", ";

						finalPath.pop();
					}

					std::cout << "and the distance is " << distance[destin];
					std::cout << std::endl;
				}

				else{

					std::cout << "No Path Exists\n";

				}

			}
			else{
				std::cout << "Vertex " << destin << " is not a valid vertex in the graph\n";
			}
		}


friend std::ostream& operator<<( std::ostream& out, const Graph<T>& g){

			for( unsigned int i = 0; i < g.vertices.size(); i++ ){

				out << "{" << i << ", " << g.vertices.at(i).get_vertex_value() << "} ";

				for( unsigned int j = 0; j < g.vertices.at(i).num_edges(); j++ ){

					Edge tempEdge = g.vertices.at(i).get_edge(j);

					std::cout << "(" << tempEdge.destin << ", ";
					std::cout << tempEdge.weight << "), ";
				}

				out << std::endl;
			}

			return out;
		}

};

#endif
