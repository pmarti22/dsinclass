/**********************************************
* File: arcsineh.cpp
* Author: Pablo Martinez Abrego
* Email: pmarti22@nd.edu
*
* Program that solves the arcsineh function for a 
* value x read in from the command line.
**********************************************/


#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream, std::stringbuf
#include <cstdlib>		// exitr
#include <cmath>		// pow

/********************************************
* Function Name  : factorial
* Pre-conditions : unsigned int i
* Post-conditions: double
*
* Calculates the factorial of i
********************************************/
double factorial(unsigned int i){

	// Base case i = 0
	if(i == 0){
		return 1;
	}

	return (double)i * factorial(i-1);
}


/********************************************
* Function Name  : getArgv1Num
* Pre-conditions : int argc, char** argv
* Post-conditions: double
*
* Returns argv[1] as a double if valid
********************************************/
double getArgv1Num(int argc, char** argv){

	if(argc != 2){
		std::cout << "Incorrect number of inputs" << std::endl;
		exit(-1);
	}

	// stringstream used for the conversion initialized with the contents of argv[1]
	double factNum;
	std::stringstream convert(argv[1]);

	//give the value to factNum using the characters in the string
	if ( !(convert >> factNum) ){
		std::cout << "Not a valid integer" << std::endl;
		exit(-1);
	}

	// If the value is < -1 or > 1
	if(factNum < -1 || factNum > 1){
		std::cout << "Value must be between -1 and 1" << std::endl;
		exit(-1);
	}

	return factNum;

}


/********************************************
* Function Name  : getArcsineh
* Pre-conditions : double value, int n
* Post-conditions: double
*
* Finds Arcsineh of value
********************************************/
double getArcsineh(double value, int n){

	// Base case for problem
	if(n > 50){
		return 0; // Ends recursive calling
	}

	// Return the value of the formula at n, and recursively call the function at n++
	return ((pow(-1, n) * factorial(2*n)) / (pow(2, 2*n) * pow(factorial(n), 2) *
		(2*n+1)) * pow(value, 2*n + 1)) + getArcsineh(value, n+1);

}


/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* This is the main driver function
********************************************/
int main(int argc, char** argv){

	// Get the value
	double value = getArgv1Num(argc, argv);

	std::cout << "Finding arcsineh of " << value << std::endl;

	int n = 0;
	double x = getArcsineh(value, n);

	std::cout << "Arcinseh of " << value << " is: " << x << std::endl;

	return 0;
}
