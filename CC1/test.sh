echo "./arcsineh 0 - Final result should be approx. 0"
./arcsineh 0
echo ""
echo "./arcsineh 1 - Final result should be approx. 0.881762"
./arcsineh 1
echo ""
echo "./arcsineh -1 - Final result should be approx. -0.881762"
./arcsineh -1
echo ""
echo "./arcsineh 0.25 - Final result should be approx. 0.247466"
./arcsineh 0.25
echo ""
echo "./arcsineh -0.25 - Final result should be approx. -0.247466"
./arcsineh -0.25
echo ""
echo "./arcsineh 0.5 - Final result should be approx. 0.481212"
./arcsineh 0.5
echo ""
echo "./arcsineh -0.5 - Final result should be approx. -0.481212"
./arcsineh -0.5