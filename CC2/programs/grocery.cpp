#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "../classes/Item.h"
#include <vector>

void getFileStream(int argc, char** argv, std::ifstream& ifs){

	// Must be exactly two characters
	if(argc != 2){
		std::cout << "Incorrect number of inputs" << std::endl;
		exit(-1);
	}

	// Attempt to open the file
	ifs.open (argv[1], std::ifstream::in);

	if(!ifs.is_open()){
		std::cout << "Input File Name " << argv[1] << " does not exist\n";
		exit(-1);
	}

}

int main(int argc, char** argv){

	// Check the input
	std::ifstream groceryListIn;
	getFileStream(argc, argv, groceryListIn);

	/***************************************
	 * Initialize an empty grocery list
	 * using your selected Data Structure
	 * between here and the while loop
	 ***************************************/

	std::vector<Item> groceryList;

	// Read in the playlist information
	while( !groceryListIn.eof() ){

		// Boolean if all checks pass
		bool checks = true;

		// Get the unsigned int from the file
		std::string readStr;

		// First, get the itemID
		std::getline(groceryListIn, readStr);
		std::stringstream convert(readStr);

		// Get all the information for a song
		unsigned int itemID;
		if( !(convert >> itemID) ){
			checks = false;
		}

		// Use getline for the entire artist's Name
		std::string manufacturer;
		std::getline(groceryListIn, manufacturer);

		// Use getline for the entire song title
		std::string itemName;
		std::getline(groceryListIn, itemName);

		// Next, get the cost and create a Money object
		std::getline(groceryListIn, readStr);
		std::stringstream convert2(readStr);
		long int dollars;
		int cents;
		if( !(convert2 >> dollars >> cents) ){
			checks = false;
		}
		Money currCost(dollars, cents);


		// If all checks pass
		if(checks == true){

			/***********************
			* You have everything you need to insert
			* into a Item class object. You just need
			* to create the object and insert into the
			* Data Structure of your choice
			***********************/

			Item item(itemID, manufacturer, itemName, currCost);
			groceryList.push_back(item);

		}

	}

	/***********************
	* Now that the playlist is created
	* print all the objects in the Data Structure
	* Then, print the final cost
	***********************/

	for(unsigned int i = 0; i < groceryList.size(); i++)
		std::cout << groceryList[i];

}
