

#include "Item.h"
#include <iomanip>

Item::Item(unsigned int id, std::string man, std::string name, Money price):
  itemID(id),
  manufacturer(man),
  itemName(name),
  cost(price) {}

  std::ostream& operator<<(std::ostream& out, Item& i){

  out << std::left << std::setw(20) << "Item ID:" << i.itemID << std::endl;
  out << std::left << std::setw(20) << "Manufacturer Name:" << i.manufacturer << std::endl;
  out << std::left << std::setw(20) << "Item Name:" << i.itemName << std::endl;
  out << std::left << std::setw(20) << "Cost: " << i.cost << std::endl << std::endl;

  return out;
}
