
#ifndef ITEM_H
#define ITEM_H

#include "Money.h"
#include <iostream>
#include <string>

class Item {

  private:
    unsigned int itemID;
    std::string manufacturer;
    std::string itemName;
    Money cost;

  public:
    Item(unsigned int, std::string, std::string, Money);
    friend std::ostream& operator<<(std::ostream &out, Item& i);
};

#endif
