/**********************************************
* File: Money.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* This contains the Money for Problem 1 Lecture 11 
**********************************************/

#include "Money.h"


Money::Money(long int dollarsIn, int centsIn) :
			dollars(dollarsIn), cents(centsIn) {}


Money Money::operator+(const Money& rhs){
	
	long int dollarSum = this->dollars + rhs.dollars;
	
	int centSum = this->cents + rhs.cents;
	
	if( centSum >= 100 ){
		centSum -= 100;
		dollarSum++;
	}
	
	return Money(dollarSum, centSum);
	
}

Money Money::operator-(const Money& rhs){
	
	long int dollarDiff = this->dollars - rhs.dollars;
	
	int centDiff = this->cents - rhs.cents;
	
	if( centDiff < 0 ){
		centDiff = 100 - centDiff;
		dollarDiff--;
	}
	
	return Money(dollarDiff, centDiff);
	
}


std::ostream& operator<<(std::ostream& out, const Money& print){
	
	out << "$" << print.dollars << ".";
	
	if(print.cents < 10){
		out << "0";
	}
	out << print.cents;
	
	return out;
}