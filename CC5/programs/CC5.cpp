#include "../classes/BSTNode.h"
#include <unordered_map>

bool validTree(BSTNode<int>* curr, int min, bool boolMin, int max, bool boolMax){

	if(curr == NULL)
		return true;

	if(curr->data == max || curr->data == min){
		std::cout << "Invalid Tree: Node containing " << curr->data << " already encountered in tree." << std::endl;
		return false;
	}

	if(curr->data < min && boolMin){
		std::cout << "Invalid Tree: " << curr->data << " is in the right subtree of " << min << std::endl;
		return false;
	}

	else if (curr->data > max && boolMax){
		std::cout << "Invalid Tree: " << curr->data << " is in the left subtree of " << max << std::endl;
		return false;
	}

	return validTree(curr->left, min, boolMin, curr->data, true) && validTree(curr->right, curr->data, true, max, boolMax);

}


int main(){

	BSTNode<int>* node1 = new BSTNode<int>(10);
	BSTNode<int>* node2 = new BSTNode<int>(5);
	BSTNode<int>* node3 = new BSTNode<int>(20);
	BSTNode<int>* node4 = new BSTNode<int>(1);
	BSTNode<int>* node5 = new BSTNode<int>(7);
	BSTNode<int>* node6 = new BSTNode<int>(15);
	BSTNode<int>* node7 = new BSTNode<int>(25);

	// Test 1 - Valid Tree
	node1->left = node2;
	node1->right = node3;
	node2->left = node4;
	node2->right = node5;
	node3->left = node6;
	node3->right = node7;

	bool valid;
	const bool boolMin = false;
	const bool boolMax = false;

	const int min = 0;
	const int max = 0;

	// Run your tests for Test 1
	valid = validTree(node1, min, boolMin, max, boolMax);

	if(valid){
		std::cout << "Test 1 Passed!" << std::endl;
	}
	else{
		std::cout << "Test 1 Failed!" << std::endl;
	}

	// Test 2 - 5 (node2) right and 20 (node3) left
	// both point to 15 (node 6)
	node2->right = node6;

	// Run your tests for Test 2
	valid = validTree(node1, min, boolMin, max, boolMax);

	if(valid){
		std::cout << "Test 2 Passed!" << std::endl;
	}
	else{
		std::cout << "Test 2 Failed!" << std::endl;
	}


	// Test 3 - 10 (node1) left points to 7 (node5),
	// and 7 (node5) right points to 5 (node2)
	// Will not pass
	node1->left = node5;
	node1->right = node3;
	node5->left = node4;
	node5->right = node2;
	node3->left = node6;
	node3->right = node7;
	node2->left = NULL;
	node2->right = NULL;

	// Run your tests for Test 3
	valid = validTree(node1, min, boolMin, max, boolMax);

	if(valid){
		std::cout << "Test 3 Passed!" << std::endl;
	}
	else{
		std::cout << "Test 3 Failed!" << std::endl;
	}



	// Test 4 - right child of 25 (node7) points at 5 (node2)
	// Will not pass
	node1->left = node2;
	node1->right = node3;
	node2->left = node4;
	node2->right = node5;
	node3->left = node6;
	node3->right = node7;
	node7->right = node2;
	node5->left = NULL;
	node5->right = NULL;

	// Run your tests for Test 4
	valid = validTree(node1, min, boolMin, max, boolMax);

	if(valid){
		std::cout << "Test 4 Passed!" << std::endl;
	}
	else{
		std::cout << "Test 4 Failed!" << std::endl;
	}

	// Test 5 - Node 7 points at node 7, creating a loop
	// Will not pass
	node7->right = node7;


	// Run your tests for Test 5
	valid = validTree(node1, min, boolMin, max, boolMax);

	if(valid){
		std::cout << "Test 5 Passed!" << std::endl;
	}
	else{
		std::cout << "Test 5 Failed!" << std::endl;
	}


	return 0;

}
