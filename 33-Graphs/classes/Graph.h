#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include "Vertex.h"

#include <iostream>
#include <fstream>

template <class T>
class Graph
{

private:
    std::vector<Vertex<T>> vertices;
    bool bi_directional;

public:
    Graph(bool bi_direct) : vertices(), bi_directional(bi_direct) {}

    void add_vertex(T vertexData)
    {

        Vertex<T> theVertex(vertexData);
        vertices.push_back(theVertex);
    }

    void add_edge(unsigned int origin, unsigned int destin, int weight)
    {

        if (origin < vertices.size() && destin < vertices.size())
        {

            vertices.at(origin).add_edge(destin, weight);

            if (bi_directional)
            {
                vertices.at(destin).add_edge(origin, weight);
            }
        }
        else
        {
            std::cout << "Unable to add edge. Edge outside graph bounds\n";
        }
    }

    bool get_vertex_value(unsigned int vertex, T &value)
    {

        bool validVertex = false;

        if (vertex < vertices.size())
        {

            validVertex = true;
            value = vertices.at(vertex).get_vertex_value();
        }

        return validVertex;
    }
};

#endif
