#ifndef VERTEX_H
#define VERTEX_H

#include <vector>
#include <iostream>
#include "Edge.h"

template <class T>
class Vertex
{

private:
    std::vector<Edge> edges;
    T data;

public:
    Vertex(const T &dataIn) : edges(), data(dataIn) {}

    T get_vertex_value() const
    {
        return data;
    }

    void set_vertex_value(const T &dataIn)
    {
        data = dataIn;
    }

    Edge get_edge(unsigned int edgeOrder) const
    {
        return edges.at(edgeOrder);
    }

    unsigned int num_edges() const
    {
        return (unsigned int)edges.size();
    }

    void add_edge(unsigned int destin, int weight)
    {

        edges.push_back(Edge(destin, weight));
    }

    T get_vertex_value() const
    {
        return data;
    }
};

#endif
