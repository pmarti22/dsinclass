#include "../classes/TrieNode.h"

#include <iostream>
#include <string>

void addString( TrieNode* theNode, const std::string& word, unsigned int value ){
	
	if( value < word.size() ){
		
		theNode->addChild( word[value] );
		
		TrieNode* nextLevel = theNode->getChildPtr( word[value] );
		
		addString( nextLevel, word, value + 1);
	}
	
	else if( value == word.size() ){
		
		theNode->addChild( '*' );
		
		return;
	}
	
}


void printTriePreOrder( TrieNode* theNode, unsigned int level ){
	
	if( theNode == NULL ){
		
		return;
		
	}
	
	// Root Node 
	if( theNode->getLetter() == 0 ){
		
		std::cout << "root: " << level << std::endl;
		
	}
	
	else{
		
		std::cout << theNode->getLetter() << " " << level << std::endl;
		
	}
	
	if( theNode->getLetter() == '*' ){
		
		return;
		
	}
	
	// If we get here, there are children 
	for(unsigned int iter = 0; iter < theNode->numChildren(); iter++){
	
		printTriePreOrder(theNode->getChildPtrOrder(iter), level + 1);
		
	}
	
}

int main(){
	
	std::string CAN("CAN");
	std::string CANDY("CANDY");
	std::string COUNT("COUNT");
	std::string COULD("COULD");
	std::string FIT("FIT");
	std::string FAST("FAST");
	std::string FASTER("FASTER");
	std::string FAT("FAT");
	std::string FOX("FOX");
	std::string FOUND("FOUND");
	
	// Create a root TrieNode
	TrieNode* root = new TrieNode();

	// Add the strings 
	addString(root, FOUND, 0);
	addString(root, COUNT, 0);
	addString(root, FIT, 0);	
	addString(root, CAN, 0);
	addString(root, FASTER, 0);
	addString(root, CANDY, 0);
	addString(root, FAST, 0);
	addString(root, COULD, 0);
	addString(root, FOX, 0);
	addString(root, FAT, 0);
		
	printTriePreOrder(root, 1);
	
	std::cout << std::endl; 
	
	return 0;
}