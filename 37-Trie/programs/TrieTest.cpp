#include "../classes/Trie.h"

#include <iostream>
#include <string>

void checkWord( const Trie& theTrie, const std::string& theWord ){
	
	std::cout << "The word " << theWord << " is";
	
	if( !theTrie.contains( theWord ) ){
		
		std::cout << " not";
	}
	
	std::cout << " in the Trie" << std::endl;
	
}


int main(){
	
	Trie theTrie;
	std::string BAT("BAT");
	std::string BAT_CAVE("BAT-CAVE");
	std::string BAT_COMPUTER("BAT-COMPUTER");
	std::string BAT_DANCE("BAT-DANCE");
	std::string BAT_DIAMOND("BAT-DIAMOND");
	std::string BAT_PHONE("BAT-PHONE");
	std::string BAT_SHARK("BAT-SHARK");
	std::string BAT_SIGNAL("BAT-SIGNAL");
	std::string BAT_TASER("BAT-TASER");
	std::string BAT_TRACER("BAT-TRACER");
	std::string BATARANG("BATARANG");
	std::string BATGIRL("BATGIRL");
	std::string BATMAN("BATMAN");
	std::string BATMOBILE("BATMOBILE");
	
	// Add Strings to the Trie
	theTrie.addString(BAT);
	theTrie.addString(BAT_CAVE);
	theTrie.addString(BAT_COMPUTER);
	theTrie.addString(BAT_DANCE);
	theTrie.addString(BAT_DIAMOND);
	theTrie.addString(BAT_PHONE);
	theTrie.addString(BAT_SHARK);
	theTrie.addString(BAT_SIGNAL);
	theTrie.addString(BAT_TASER);
	theTrie.addString(BAT_TRACER);
	theTrie.addString(BATARANG);
	theTrie.addString(BATGIRL);
	theTrie.addString(BATMAN);
	theTrie.addString(BATMOBILE);
	
	// Print the Pre-Order Traversal
	theTrie.printTriePreOrder();
	
	// Test Contains
	checkWord(theTrie, std::string("BA") );
	checkWord(theTrie, std::string("BAT") );
	checkWord(theTrie, std::string("BATMAN") );
	checkWord(theTrie, std::string("BATMEN") );	
	checkWord(theTrie, std::string("BAT-SIGNAL") );
	checkWord(theTrie, std::string("BATGIRL") );
	checkWord(theTrie, std::string("RIDDLER") );
	checkWord(theTrie, std::string("BATMOBILE") );
	checkWord(theTrie, std::string("BAT-COMPUTER") );
	checkWord(theTrie, std::string("BAT-COMP") );
	checkWord(theTrie, std::string("JOKER") );
	checkWord(theTrie, std::string("BAT-CAVE") );
	checkWord(theTrie, std::string("BAT-CAVES") );	
	
	return 0;
}