#include "../classes/Trie.h"

#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>


void getFileStream(int argc, char** argv, std::ifstream& ifs){

	// Must be exactly two characters
	if(argc != 2){
		std::cout << "Incorrect number of inputs" << std::endl;
		exit(-1);
	}

	// Attempt to open the file
	ifs.open (argv[1], std::ifstream::in);

	if(!ifs.is_open()){
		std::cout << "Input File Name " << argv[1] << " does not exist\n";
		exit(-1);
	}

}

void wordBreak( Trie& dictTrie, const std::string& str, int size, std::string result){

	for(int iter = 1; iter <= size; iter++){

		std::string prefix = str.substr(0, iter);

		if(dictTrie.contains( prefix )) {

			if(iter == size){

				result += str.substr(0, iter);
				std::cout << result << std::endl;
				return;
			}

			wordBreak(dictTrie, str.substr(iter, size-iter), (int)(size-iter),
				std::string(result + prefix + " ") );

		}

	}

}

void wordBreak( Trie& dictTrie, const std::string& str) {

	wordBreak(dictTrie, str, (int)str.size(), std::string(""));
}


int main(int argc, char** argv){

	// Check the input
	std::ifstream dictionaryIn;
	getFileStream(argc, argv, dictionaryIn);

	Trie dictTrie;

	// Read in the dictionary information
	std::cout << "Reading in dictionary, this will take about 20 seconds...\n";
	while( !dictionaryIn.eof() ){

		// Get the unsigned int from the file
		std::string word;

		dictionaryIn >> word;

		dictTrie.addString(word);

	}


	// Tests
	wordBreak(dictTrie, std::string(""));
	wordBreak(dictTrie, std::string("ILIKEPIZZA"));
	wordBreak(dictTrie, std::string("APPLEPENAPPLE"));
	wordBreak(dictTrie, std::string("FGJGKDJSKKSGJFJFJSFJK"));
	wordBreak(dictTrie, std::string("CANTELOPE"));
	wordBreak(dictTrie, std::string("WAKEUPTHEECHOES"));
	wordBreak(dictTrie, std::string("ITWASTHEBESTOFTIMESITWASTHEWORSTOFTIMES"));

	return 0;
}
