#ifndef GRAPH_H
#define GRAPH_H

#include <vector>
#include "Vertex.h"

#include <iostream>
#include <fstream>

#include <queue>
#include <stack>

#include "MSTElem.h"
#include <unordered_map>
#include <set>

template<class T>
class Graph{

	private:

		std::vector< Vertex< T > > vertices;
		bool bi_directional;

	public:

		Graph( bool bi_direct ) : vertices(), bi_directional(bi_direct) {}


		void add_vertex( T vertexData ){

			Vertex<T> theVertex( vertexData );

			vertices.push_back( theVertex );

		}


		void add_edge(unsigned int origin, unsigned int destin, int weight ){

			if( origin < vertices.size() && destin < vertices.size() ){

				vertices.at(origin).add_edge( destin, weight );

				if( bi_directional ){

					vertices.at(destin).add_edge( origin, weight );
				}
			}
			else{
				std::cout << "Unable to add edge. Edge outside graph bounds\n";
			}
		}


		bool get_vertex_value( unsigned int vertex, T& value ){

			bool validVertex = false;

			if( vertex < vertices.size() ){

				validVertex = true;
				value = vertices.at(vertex).get_vertex_value();

			}

			return validVertex;

		}


		void set_vertex_value( unsigned int vertex, T& value ){

			if( vertex < vertices.size() ){

				value = vertices.at(vertex).set_vertex_value( value );

			}

		}

		bool adjacent( unsigned int origin, unsigned int destin ){

			if( origin < vertices.size() && destin < vertices.size() ){

				for(unsigned int i = 0; i < vertices.at(origin).num_edges(); i++){

					if(vertices.at(origin).get_edge().destin == destin){

						return true;
					}
				}

				return false;
			}
			else{

				return false;
			}

		}


		void neighbors( unsigned int vertex ){

			if( vertex < vertices.size() ){

				std::cout << "Vertex " << vertex << " had edges going to: ";

				for(unsigned int i = 0; i < vertices.at(vertex).num_edges(); i++){

					std::cout << vertices.at(vertex).get_edge().destin << ", ";

				}
				std::cout << std::endl;

			}
			else{

				std::cout << vertex << " exceeds graph capacity\n";
			}

		}


		bool get_edge_value( unsigned int origin, unsigned int destin,
				int& weight){

			if( origin < vertices.size() && destin < vertices.size() ){

				vertices.at(origin).get_edge_value( destin, weight );
				return true;

			}

			return false;

		}

		bool set_edge_value( unsigned int origin, unsigned int destin,
				int weight){

			if( origin < vertices.size() && destin < vertices.size() ){

				vertices.at(origin).set_edge_value( destin, weight );

				if( bi_directional ) {

					vertices.at(destin).set_edge_value( origin, weight );

				}

				return true;

			}

			return false;

		}


		void remove_edge( unsigned int origin, unsigned int destin){

			if( origin < vertices.size() && destin < vertices.size() ){

				vertices.at(origin).remove_edge( destin );

				if( bi_directional ) {

					vertices.at(destin).remove_edge( origin );

				}

				return;

			}

			return;

		}


		void remove_vertex( unsigned int vertexLoc ){

			if( vertexLoc < vertices.size() ){

				vertices.erase( vertices.begin() + vertexLoc );

				for(unsigned int i = 0; i < vertices.size(); i++){
					vertices.at(i).removeAndUpdateEdges( vertexLoc );
				}

			}

		}


		void BFS( unsigned int destin ){

			// Check if the graph is valid to be searched
			if( destin < vertices.size() && vertices.size() > 0){

				std::queue< unsigned int > theQueue;
				unsigned int* parents = new unsigned int[vertices.size()];
				bool* visited = new bool[vertices.size()];
				std::stack< unsigned int > finalPath;

				// Initialize the search
				bool found = false;
				theQueue.push(0);
				parents[0] = -1;
				visited[0] = true;

				if( destin == 0 ){

					found = true;
					finalPath.push( 0 );

				}

				// Conduct the search. If the queue's size matches the vertices
				// then every vertex has been visited.
				while( !found && theQueue.size() < vertices.size() ){

					unsigned int vertex = theQueue.front();
					theQueue.pop();

					for( unsigned int i = 0; i < vertices.at( vertex ).num_edges(); i++){

						Edge tempEdge = vertices.at( vertex ).get_edge( i );

						// State the vertex has been visited
						if( !visited[ tempEdge.destin ] ){
							visited[ tempEdge.destin ] = true;
							parents[ tempEdge.destin ] = vertex;

							if( tempEdge.destin == destin ){

								found = true;
								finalPath.push( destin );
								break;
							}
							else{
								theQueue.push(tempEdge.destin);
							}
						}
					}

				}


				if( found ){

					unsigned int parent = parents[ destin ];

					while( parent != -1 ){

						finalPath.push( parent );
						parent = parents[ parent ];

					}

					// Finally, print the BFS Path
					std::cout << "The path from " << vertices.at( 0 ).get_vertex_value();
					std::cout << " to " << vertices.at(destin).get_vertex_value() << " is: ";

					while(!finalPath.empty()){

						std::cout << vertices.at( finalPath.top() ).get_vertex_value() << ", ";

						finalPath.pop();
					}

					std::cout << std::endl;
				}

				else{

					std::cout << "No Path Exists\n";

				}

			}
			else{

				std::cout << "Vertex " << destin << " is not a valid vertex in the graph\n";

			}

		}


		Graph<T> MST(){

			Graph<T> MSTGraph( false );

			std::priority_queue< MSTElem > MST_PQ;
			std::unordered_map< unsigned int, bool > frontier;
			int* weights = new int[vertices.size()];
			unsigned int* parents = new unsigned int[vertices.size()];

			// First, fill the Priority Queue with MSTElem values
			// Origin will be 0
			MSTElem origin( 0, 0 );
			MST_PQ.push( origin );
			frontier.insert( {0, true} );
			parents[ 0 ] = -1;
			weights[ 0 ] = -2147483648;

			// Make the other elements infinity
			for(unsigned int i = 1; i < vertices.size(); i++){

				MSTElem temp( i, 2147483647 );
				weights[ i ] = 2147483647;
				MST_PQ.push( temp );
				frontier.insert( {i, true} );
			}


			while( !MST_PQ.empty() ){

				MSTElem currElem = MST_PQ.top();
				Vertex<T> currVert = vertices.at( currElem.index );
				MST_PQ.pop();

				frontier[ currElem.index ] = false;

				for(unsigned int i = 0; i < currVert.num_edges(); i++){

					Edge currEdge = currVert.get_edge( i );

					if( currEdge.weight < weights[ currEdge.destin ] && frontier[ currEdge.destin ] ){

						weights[ currEdge.destin ] = currEdge.weight;

						parents[ currEdge.destin ] = currElem.index;

						MSTElem pushElem( currEdge.destin, currEdge.weight );

						MST_PQ.push( pushElem );
					}

				}
			}

			// Add the vertext values for each element
			for( unsigned int i = 0; i < vertices.size(); i++){

				MSTGraph.add_vertex( vertices.at(i).get_vertex_value() );

			}

			// Add the edges to the MSTGraph to be returned
			for( unsigned int i = 1; i < vertices.size(); i++){

				if( parents[i] != -1 ){

					MSTGraph.add_edge( parents[i], i, weights[i] );

				}
			}

			return MSTGraph;

		}


		void DFS( unsigned int destin ){

			// Check if the graph is valid to be searched
			if( destin < vertices.size() && vertices.size() > 0){

				std::stack< unsigned int > theStack;
				std::vector<unsigned int> parents(vertices.size(), -1);
				std::vector<unsigned int> visited_edges(vertices.size(), 0);
				std::vector<bool> visited(vertices.size(), false);
				std::vector<int> weights(vertices.size());
				std::stack< unsigned int > finalPath;
				bool found = false;

				// Initialize the search
				theStack.push(0);
				parents[0] = -1;
				visited[0] = true;
				unsigned int visitedVerts = 1;

				if( destin == 0 ){

					found = true;
					finalPath.push( 0 );

				}

				// Conduct the search. If the stack's size matches the vertices
				// then every vertex has been visited.
				while( !found && visitedVerts < vertices.size()){

					unsigned int vertex = theStack.top();

					if( vertices.at( vertex ).num_edges() > visited_edges[ vertex ] ){

						Edge tempEdge = vertices.at( vertex ).get_edge( visited_edges[ vertex ] );

						visited_edges[ vertex ]++;

						// State the vertex has been visited
						if( !visited[ tempEdge.destin ] ){

							visited[ tempEdge.destin ] = true;
							parents[ tempEdge.destin ] = vertex;
							weights[ tempEdge.destin ] = tempEdge.weight;

							if( tempEdge.destin == destin ){

								found = true;
								finalPath.push( destin );
								break;
							}
							else{
								theStack.push(tempEdge.destin);
							}

							visitedVerts++;

						}

						vertex = theStack.top();
					}

					if( visited[vertex] &&  visited_edges [vertex] == vertices.at( vertex ).num_edges() ){
						theStack.pop();
					}

					if( theStack.empty() ){

						break;
					}

				}


				if( found ){

					unsigned int parent = parents[ destin ];

					while( parent != -1 ){

						finalPath.push( parent );
						parent = parents[ parent ];

					}

					// Finally, print the BFS Path
					std::cout << "The path from " << vertices.at( 0 ).get_vertex_value();
					std::cout << " to " << vertices.at(destin).get_vertex_value() << " is: ";

					while(!finalPath.empty()){

						std::cout << vertices.at( finalPath.top() ).get_vertex_value() << ", ";

						finalPath.pop();
					}

					std::cout << std::endl;
				}

				else{

					std::cout << "No Path Exists\n";

				}

			}
			else{

				std::cout << "Vertex " << destin << " is not a valid vertex in the graph\n";

			}

		}


		Graph<T> TopSort( ){

			// Graph with bi-directional set as false
			Graph<T> topSort( false );

			std::stack< unsigned int > theStack;
			std::vector<unsigned int> parents(vertices.size(), -1);
			std::vector<unsigned int> visited_edges(vertices.size(), 0);
			std::vector<bool> visited(vertices.size(), false);
			std::vector<int> weights(vertices.size());

			// Initialize the search
			theStack.push(0);
			parents[0] = -1;
			visited[0] = true;
			unsigned int visitedVerts = 0;

			// Conduct the search. If the stack's size matches the vertices
			// then every vertex has been visited.
			while( visitedVerts < vertices.size() ){

				unsigned int vertex = theStack.top();

				if( vertices.at( vertex ).num_edges() > visited_edges[ vertex ] ){

					Edge tempEdge = vertices.at( vertex ).get_edge( visited_edges[ vertex ] );

					visited_edges[ vertex ]++;

					// State the vertex has been visited
					if( !visited[ tempEdge.destin ] ){

						visited[ tempEdge.destin ] = true;
						parents[ tempEdge.destin ] = vertex;
						weights[ tempEdge.destin ] = tempEdge.weight;

						theStack.push(tempEdge.destin);

						visitedVerts++;

					}

					vertex = theStack.top();
				}

				//if( visited[vertex] &&  visited_edges [vertex] == vertices.at( vertex ).num_edges() ){
				if( visited_edges [vertex] == vertices.at( vertex ).num_edges() ){
					theStack.pop();
				}

				if( theStack.empty() ){

					if( visitedVerts < vertices.size() ){

						unsigned int temp = vertex + 1;

						while( visited[ temp ] ){

							temp++;
						}

						theStack.push( temp );
						visitedVerts++;

					}

					else{
						break;
					}
				}

			}

			// Add the vertices
			for( unsigned int i = 0; i < vertices.size(); i++){

				topSort.add_vertex( vertices.at(i).get_vertex_value() );

			}

			for( unsigned int i = 0; i < vertices.size(); i++){

				if( parents[i] != -1 ){
					topSort.add_edge( parents[i], i, weights[i] );
				}

			}

			return topSort;


			}


			void Dijkstra( unsigned int destin ){


				if( destin < vertices.size() && vertices.size() > 0 ){

					std::stack< unsigned int > theStack;
					std::vector< unsigned int > parents( vertices.size(), -1 );
					std::vector< int > distance( vertices.size(), 2147483647 );
					std::stack< unsigned int > finalPath;
					bool found = false;

					// Initialize the Origin
					theStack.push( 0 );
					distance[ 0 ] = 0;

					if( destin == 0 ){

						found = true;
						finalPath.push( 0 );
					}


					while( !theStack.empty() ){

						unsigned int index = theStack.top();
						theStack.pop();

						for(unsigned int i = 0; i < vertices.at( index).num_edges(); i++){

							Edge tempEdge = vertices.at( index ).get_edge( i );

							if( distance[ index ] + tempEdge.weight < distance[ tempEdge.destin ] ){

								distance[ tempEdge.destin ] = distance[ index ] + tempEdge.weight;
								parents[ tempEdge.destin ] = index;

								if( tempEdge.destin == destin && !found ){

									found = true;
									finalPath.push( destin );
								}

								theStack.push( tempEdge.destin );
							}

						}

					}

					if( found ){

						unsigned int parent = parents[ destin ];

						while( parent != -1 ){

							finalPath.push( parent );
							parent = parents[ parent ];

						}

						// Finally, print the Dijkstra
						std::cout << "The path from " << vertices.at( 0 ).get_vertex_value();
						std::cout << " to " << vertices.at(destin).get_vertex_value() << " is: ";

						while(!finalPath.empty()){

							std::cout << vertices.at( finalPath.top() ).get_vertex_value() << ", ";

							finalPath.pop();
						}

						std::cout << "and the distance is " << distance[ destin ];

						std::cout << std::endl;
					}

					else{

						std::cout << "No Path Exists\n";

					}

				}
				else{

					std::cout << "Vertex " << destin << " is not a valid vertex in the graph\n";
				}


			}


			friend std::ostream& operator<<( std::ostream& out, const Graph<T>& g){

				for( unsigned int i = 0; i < g.vertices.size(); i++ ){

					out << "{" << i << ", " << g.vertices.at(i).get_vertex_value() << "} ";

					for( unsigned int j = 0; j < g.vertices.at(i).num_edges(); j++ ){

						Edge tempEdge = g.vertices.at(i).get_edge(j);

						std::cout << "(" << tempEdge.destin << ", ";
						std::cout << tempEdge.weight << "), ";
					}

					out << std::endl;
				}

				return out;
			}


			void remove_cycles(){
				for(int i = 0; i < vertices.size();i++)
					vertices.at(i).delete_all_edges();
				//			int data;
				for(int i = 0; i<vertices.size()-1;i++)
				{		
					vertices.at(i).add_edge(i+1, 1);
				}
			}


		};

#endif
