#include "../classes/Graph.h"
#include <string>
#include <iostream>
#include <vector>

/********************************************
* Function Name  : createGraph1
* Pre-conditions : Graph<int>& graph1
* Post-conditions: none
*
* Creates graph1 with a call by reference
********************************************/
void createGraph1( Graph<int>& graph1 ){

	graph1.add_vertex( 4 );
	graph1.add_vertex( 20 );
	graph1.add_vertex( 5 );
	graph1.add_vertex( 10 );
	graph1.add_vertex( 30 );
	graph1.add_vertex( 18 );


	// Nodes from 0
	graph1.add_edge( 0, 1, 1 );
	graph1.add_edge( 0, 2, 1 );

	// Nodes from 1
	graph1.add_edge( 1, 3, 1 );
	graph1.add_edge( 1, 4, 1 );

	// Nodes from 2
	graph1.add_edge( 2, 3, 1 );

	// Nodes from 3
	graph1.add_edge( 3, 5, 1 );

	// Nodes from 4
	graph1.add_edge( 4, 5, 1 );

}


/********************************************
* Function Name  : createGraph2
* Pre-conditions : Graph<int>& graph2
* Post-conditions: none
*
* Creates graph2 with a call by reference
********************************************/
void createGraph2( Graph<int>& graph2 ){

	graph2.add_vertex( 3 );	// Node 0
	graph2.add_vertex( 1 ); // Node 1
	graph2.add_vertex( 2 );	// Node 2
	graph2.add_vertex( 4 ); // Node 3
	graph2.add_vertex( 5 );	// Node 4
	graph2.add_vertex( 6 );	// Node 5
	graph2.add_vertex( 7 );	// Node 6
	graph2.add_vertex( 8 );	// Node 7

	// Nodes from 0 (value 3)
	graph2.add_edge( 0, 1, 1 );
	graph2.add_edge( 0, 4, 1 );

	// Nodes from 1 (value 1)
	graph2.add_edge( 1, 6, 1 );

	// Nodes from 2 (value 2)
	graph2.add_edge( 2, 5, 1 );

	// Nodes from 3 (value 4)
	graph2.add_edge( 3, 5, 1 );

	// Nodes from 4 (value 5)
	graph2.add_edge( 4, 2, 1 );
	graph2.add_edge( 4, 3, 1 );

	// Nodes from 5 (value 6)
	graph2.add_edge( 5, 7, 1 );

	// Nodes from 6 (value 7)
	graph2.add_edge( 6, 2, 1 );
	graph2.add_edge( 6, 7, 1 );

	// Nodes from 7
	graph2.add_edge( 7, 0, 1 );	// Cycle back to Node 0
	graph2.add_edge( 7, 6, 1 ); // Cycle back to Node 6

}


/********************************************
* Function Name  : createGraph3
* Pre-conditions : Graph<char>& graph3
* Post-conditions: none
*
* Creates graph3 with a call by reference
********************************************/
void createGraph3( Graph<char>& graph3 ){

	graph3.add_vertex( 'A' );	// Node 0

	graph3.add_edge( 0, 0, 1 );	// Cycle back to Node 0


}



/********************************************
* Function Name  : createGraph4
* Pre-conditions : Graph<std::string>& graph4
* Post-conditions: none
*
* Creates graph4 with a call by reference
********************************************/
void createGraph4( Graph<std::string>& graph4 ){

	graph4.add_vertex( "Wake" );
	graph4.add_vertex( "up" );
	graph4.add_vertex( "the" );
	graph4.add_vertex( "echoes" );

	graph4.add_edge( 0, 1, 1 );
	graph4.add_edge( 0, 2, 1 );
	graph4.add_edge( 0, 3, 1 );
	graph4.add_edge( 1, 0, 1 );
	graph4.add_edge( 1, 2, 1 );
	graph4.add_edge( 1, 3, 1 );
	graph4.add_edge( 2, 0, 1 );
	graph4.add_edge( 2, 1, 1 );
	graph4.add_edge( 2, 3, 1 );
	graph4.add_edge( 3, 0, 1 );
	graph4.add_edge( 3, 1, 1 );
	graph4.add_edge( 3, 2, 1 );
}



/********************************************
* Function Name  : createGraph5
* Pre-conditions : Graph< double >& graph5
* Post-conditions: none
*
* Creates graph5 with a call by reference
********************************************/
void createGraph5( Graph< double >& graph5 ){

	graph5.add_vertex( 3.14159 );
	graph5.add_vertex( 6.28318 );
	graph5.add_vertex( 9.42477 );

	graph5.add_edge(0,0,1);
	graph5.add_edge(0,1,1);
	graph5.add_edge(1,2,1);
	graph5.add_edge(2,0,1);

}


/********************************************
* Function Name  : main
* Pre-conditions : none
* Post-conditions: int
*
* This is the main driver function for the program
********************************************/
int main(){

	std::cout << "Test 1 ----------------------------------\n";

	Graph< int > graph1( false );
	createGraph1( graph1 );

	// Print original graph:
	std::cout << "Original Graph:" << std::endl;
	std::cout << graph1 << std::endl;

	// Print Deleted edges
	std::cout << "Deleted Edges:" << std::endl;
	graph1.remove_cycles();
	//Print Modified Graph
	std::cout << "Modified Graph:" << std::endl;
	std::cout << graph1 << std::endl;


	std::cout << "Test 2 ----------------------------------\n";
	Graph< int > graph2( false );
	createGraph2( graph2 );

	// Print original graph:
	std::cout << "Original Graph:" << std::endl;
	std::cout << graph2 << std::endl;

	// Print Deleted edges
	std::cout << "Deleted Edges:" << std::endl;
	graph2.remove_cycles();
	//Print Modified Graph
	std::cout << "Modified Graph:" << std::endl;
	std::cout << graph2 << std::endl;


	std::cout << "Test 3 ----------------------------------\n";
	Graph< char > graph3( false );
	createGraph3( graph3 );

	// Print original graph:
	std::cout << "Original Graph:" << std::endl;
	std::cout << graph3 << std::endl;

	// Print Deleted edges
	std::cout << "Deleted Edges:" << std::endl;
	graph3.remove_cycles();
	//Print Modified Graph
	std::cout << "Modified Graph:" << std::endl;
	std::cout << graph3 << std::endl;


	std::cout << "Test 4 ----------------------------------\n";
	Graph< std::string > graph4( false );
	createGraph4( graph4 );

	// Print original graph:
	std::cout << "Original Graph:" << std::endl;
	std::cout << graph4 << std::endl;

	// Print Deleted edges
	std::cout << "Deleted Edges:" << std::endl;
	graph4.remove_cycles();
	//Print Modified Graph
	std::cout << "Modified Graph:" << std::endl;
	std::cout << graph4<< std::endl;


	std::cout << "Test 5 ----------------------------------\n";
	Graph< double > graph5( false );
	createGraph5( graph5 );

	// Print original graph:
	std::cout << "Original Graph:" << std::endl;
	std::cout << graph5 << std::endl;

	// Print Deleted edges
	std::cout << "Deleted Edges:" << std::endl;
	graph5.remove_cycles();
	//Print Modified Graph
	std::cout << "Modified Graph:" << std::endl;
	std::cout << graph5<< std::endl;


	return 0;
}
