// fractals.cpp
#include "../classes/fracFunc.h"

// Code Segment 6 - fillQueue method goes here
void fillQueue(std::ifstream& input, std::queue<char>& inputQueue){
	char temp;

	while(input >> temp){
		inputQueue.push(temp);
	}
}


int main(int argc, char**argv) {

	if(argc != 2){

		std::cout << "Incorrect Number of Inputs" << std::endl;
		exit(-1);

	}

   int cx = WINDOW_W/2, cy = WINDOW_H/2, length = WINDOW_W/2;
   float radius;
   int x1, y1, x2, y2, x3, y3;

	// Code Segment 7 Goes Here
	std::queue<char> inputQueue;
	std::ifstream input(argv[1]);

	if(!input){

		std::cout << "Invalid File Name " << argv[1] << std::endl;
		exit(-1);

	}

	fillQueue(input, inputQueue);


   char NDBlue[] = "#0C2340";	// Official Notre Dame Color for Blue

  // Open a new window for drawing.
  gfx_open(WINDOW_W, WINDOW_H, "Fractals", NDBlue);

	// Code Segment 8
	while ( !inputQueue.empty() ) {
		char c = inputQueue.front();
		inputQueue.pop();

		gfx_clear();
		gfx_color(201,151,0);

      switch (c) {
         case '1':
			 // Sierpinski Triangle
			 x1 = INSET;
			 y1 = INSET;
			 x2 = WINDOW_W - INSET;
			 y2 = INSET;
			 x3 = (x1 + x2) / 2;
			 y3 = WINDOW_H - INSET;
			 drawSierpinski(x1, y1, x2, y2, x3, y3);
			 break;

         // Shrinking Squares
         case '2':
			 drawShrinkingSquares(cx, cy, length);
			 break;

         // Circular Lace
         case '3':
			 radius = WINDOW_H / 3;
			 drawShrinkingCircles(cx, cy, radius);
			 break;

         // Snowflake
         case '4':
			 radius = WINDOW_H / 3;
			 drawSnowflake(cx, cy, radius);
			 break;

         // Tree
         case '5':
			 x1 = WINDOW_W / 2;
			 y1 = WINDOW_H - INSET;
			 length = WINDOW_H / 3;
			 drawTreeBranch(x1, y1, (float)length, (float)M_PI / 2, (float)0.6);
			 break;

         // Fern
         case '6':
			 x1 = WINDOW_W / 2;
			 y1 = WINDOW_H - INSET;
			 length = WINDOW_H / 6;
			 drawFern(x1, y1, (float)length, (float)M_PI / 2, (float)0.6);
			 break;

         // Spiral of spirals
         case '7':
			 x1 = WINDOW_W / 2;
			 y1 = WINDOW_H / 2;
			 drawLogSpirals(x1, y1, (float)(12 * M_PI));
			 break;

		 case '8':
			drawNDLogo();
			break;

         default:
			break;

      }

      // Updates the screen on click
      gfx_wait();

   }
   return 0;
}
