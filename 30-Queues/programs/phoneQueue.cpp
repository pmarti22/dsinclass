#include <string>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <queue>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <thread>
#include <chrono>

// Global Variables
std::mutex cout_mtx;
bool incoming = true;
thread_local int value = 1;
int adder = 0;

struct phoneCall{

	std::string phoneNum;
	int len;

	phoneCall() : phoneNum(), len() {}

	phoneCall( std::string phoneIn, int lenIn ) : phoneNum(phoneIn), len(lenIn) {}

};

class CopenhagenQueue{

	private:

		std::queue< phoneCall > phone_calls;
		std::mutex m;
		std::condition_variable cond_var;
		bool done;
		bool notified;

	public:

		CopenhagenQueue() :
			phone_calls(),
			m(),
			cond_var(),
			done(false),
			notified(false) {}

		long unsigned int size(){

			m.lock();
			long unsigned int theSize = phone_calls.size();
			m.unlock();

			return theSize;
		}

		bool empty(){

			m.lock();
			bool isEmpty = phone_calls.empty();
			m.unlock();

			return isEmpty;
		}

		void push( phoneCall inputVal ){

			m.lock();

			phone_calls.push( inputVal );
			notified = true;

			m.unlock();
		}

		void checkAndPop( bool& check, phoneCall& theCall ){

			m.lock();

			if( phone_calls.empty() ){
				check = false;
			}
			else {
				check = true;
				theCall = phone_calls.front();
				phone_calls.pop();
				notified = false;
			}


			m.unlock();
		}

		void close(){

			done = true;
			notified = true;
			cond_var.notify_one();

		}

};

int main(int argc, char** argv){

	CopenhagenQueue queue;
	std::vector< std::thread > theThreads;

	unsigned int numOperators = 4;

	srand( (unsigned int)time(NULL) );

	if(argc != 2){

		std::cout << "Incorrect Number of Inputs" << std::endl;
		exit(-1);

	}

	std::ifstream inFile(argv[1]);

	if(!inFile){

		std::cout << "Invalid File Name " << argv[1] << std::endl;
		exit(-1);

	}

	std::thread producer( [&]() {

		std::string temp;
		int len;

		while( inFile >> temp && inFile >> len){

			phoneCall newCall(temp, len);

			long unsigned int waitTime = rand()%20;

			std::this_thread::sleep_for( std::chrono::milliseconds(waitTime) );
			queue.push(newCall);

			cout_mtx.lock();
			std::cout << "After waiting " << waitTime;
			std::cout << " minutes, a new incoming phone number: ";
			std::cout << newCall.phoneNum;
			std::cout << ", Number of waiting calls - " << queue.size() << "\n";
			cout_mtx.unlock();

		}

		while( !queue.empty() ){

			incoming = true;
		}

		incoming = false;

		queue.close();

	});


	for(unsigned int i = 0; i < numOperators; i++){

		std::thread consumer( [&]() {

			adder++;
			value = adder;

			cout_mtx.lock();
			std::cout << "Operator " << value << " clocking into work...\n";
			cout_mtx.unlock();

			while(incoming){

				bool check = false;
				phoneCall theCall;

				queue.checkAndPop( check, theCall );

				if(check){

					cout_mtx.lock();
					std::cout << "Operator " << value;
					std::cout << " assigned " << theCall.phoneNum << "\n";
					cout_mtx.unlock();

					std::this_thread::sleep_for( std::chrono::milliseconds(theCall.len ));

					cout_mtx.lock();
					std::cout << "Operator " << value;
					std::cout << " - Patched through " << theCall.phoneNum;
					std::cout << " for " << theCall.len << " minutes\n";
					cout_mtx.unlock();

				}

			}

			queue.close();

		});

		theThreads.push_back( std::move(consumer) );

	}

	producer.join();

	for(unsigned int i = 0; i < numOperators; i++){

		theThreads.at(i).join();

	}



	return 0;
}
