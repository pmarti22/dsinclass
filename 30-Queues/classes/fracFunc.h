#ifndef FRACFUNC_H
#define FRACFUNC_H

#include "gfx.h"

#include <cmath>
#include <queue>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <chrono>

#define WINDOW_W 900
#define WINDOW_H 900
#define INSET 10

#define WINDOW_W 900
#define WINDOW_H 900
#define INSET 10

// functions to draw individual shapes
void drawTriangle(int x1, int y1, int x2, int y2, int x3, int y3);

void drawSquare(int cx, int cy, int length);

// recursive fractal funtions
void drawSierpinski(int x1, int y1, int x2, int y2, int x3, int y3) ;

void drawShrinkingSquares(int cx, int cy, int length);

void drawSpiralSquares(int cx, int cy, float theta, float radius);

void drawShrinkingCircles(int cx, int cy, float radius);

void drawSnowflake(int cx, int cy, float radius);

void drawTreeBranch(int x, int y, float length, float centerTheta, float angleOffset);

void drawFern(int x, int y, float length, float centerTheta, float angleOffset);

void drawLogSpirals(int cx, int cy, float maxTheta);

void drawNDLogo();

#endif