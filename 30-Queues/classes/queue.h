#ifndef QUEUE_H
#define QUEUE_H

#include <list>

template<class T>
class queue{

	private:

		std::list<T> list;
		unsigned int max_len;
		unsigned int queueSize;

	public:

		// Default constructor
		queue() : list(), max_len(-1), queueSize( 0 ) {}

		// Constructor with limited queue size
		queue(unsigned int max) : list(), max_len(max), queueSize( 0 ) {}

		unsigned int max_size(){
			return max_len;
		}

		unsigned int size(){
			return queueSize;
		}

		bool empty(){
			return queueSize == 0;
		}

		void push(T elem){

			if(queueSize < max_len){

				list.push_back( elem );
				queueSize++;

			}
		}

		void pop(){

			if(queueSize > 0){

				list.pop_front();
				queueSize--;

			}
		}

		T& front(){
			return list.front();
		}

		T& back(){
			return list.back();
		}



};

#endif
